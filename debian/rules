#!/usr/bin/make -f
# debian/rules for mrbayes
# Andreas Tille <tille@debian.org>
# GPL

# Uncomment this to turn on verbose mode.
#export DH_VERBOSE=1

export DEB_BUILD_MAINT_OPTIONS=hardening=+all

%:
	dh $@

override_dh_auto_configure:
	dh_auto_configure -- --with-beagle=/usr --enable-mpi=yes --enable-debug=yes

override_dh_auto_build:
	# build mb-mpi (from initial config)
	LIBS="-Wl,--as-needed" dh_auto_build
	mv src/mb ./mb-mpi
	# Now - build mb
	( cd src ; make clean )
	dh_auto_configure -- --with-beagle=/usr --enable-mpi=no --enable-debug=yes
	LIBS="-Wl,--as-needed" dh_auto_build
	mv src/mb .

override_dh_installexamples:
	dh_installexamples
	find debian -name "Makefile*" -delete

override_dh_compress:
	dh_compress --exclude=.pdf

override_dh_installdeb:
	# FIXME: That's a dirty hack by simply removing debug information in mrbayes-mpi-dbgsym
	# Due to the duplicated build above this is just a duplicate of mrbayes-dbgsym and creates a file conflict
	if [ -d debian/.debhelper/mrbayes-mpi/dbgsym-root/usr/lib/debug ] ; then \
	    find debian/.debhelper/mrbayes-mpi/dbgsym-root/usr/lib/debug -name "*.debug" -delete ; \
	fi
	dh_installdeb

override_dh_missing:
	dh_missing --list-missing
